public class Temperature {

    private double cTemp;

    public Temperature(){
        this.cTemp = 45;
    }
    
    public Temperature(double c){
        this.cTemp = c;   
    }

    public void setCTemp( double c){
        this.cTemp = c;
    }

    public double getCTemp( double c){
        return this.cTemp;
    }

    public double getFTemp( double c){
        return this.cTemp * 1.8 + 32;
    }

    public double getKTemp( double c){
        return this.cTemp*274.15;
    }

    public void main(String[] args){
        
    }
}
